<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7BaseUrl;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Determines the base url of a {@see ServerRequestInterface} and adds the information to the
 * request object as attributes.
 *
 * Implements the PSR-15 middleware interface ({@see MiddlewareInterface}) and can also
 * be used as a middleware using the following signature:
 *
 * ResponseInterface $response = $middleware(ServerRequestInterface $request, callable $next)
 *
 * @api usage
 * @since 2.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class RequestBaseUrlFinderMiddleware implements MiddlewareInterface
{
    /**
     * Name of the attribute the base url will be stored in.
     */
    private string $baseUrlAttribute;

    /**
     * Name of the attribute the base path will be stored in.
     */
    private string $basePathAttribute;

    /**
     * Name of the attribute the uri path will be stored in.
     */
    private string $uriPathAttribute;

    /**
     * Constructor.
     *
     * @param string $baseUrlAttribute
     *        Name of the attribute the base url will be stored in.
     * @param string $basePathAttribute
     *        Name of the attribute the base path will be stored in.
     * @param string $uriPathAttribute
     *        Name of the attribute the uri path will be stored in.
     * @no-named-arguments
     */
    public function __construct(string $baseUrlAttribute, string $basePathAttribute, string $uriPathAttribute)
    {
        $this->baseUrlAttribute = $baseUrlAttribute;
        $this->basePathAttribute = $basePathAttribute;
        $this->uriPathAttribute = $uriPathAttribute;
    }

    /**
     * @return array{baseUrlAttribute: string, basePathAttribute: string, uriPathAttribute: string}
     */
    public function __serialize(): array
    {
        return [
            'baseUrlAttribute' => $this->baseUrlAttribute,
            'basePathAttribute' => $this->basePathAttribute,
            'uriPathAttribute' => $this->uriPathAttribute,
        ];
    }

    /**
     * @param array{baseUrlAttribute: string, basePathAttribute: string, uriPathAttribute: string} $data
     */
    public function __unserialize(array $data): void
    {
        $this->baseUrlAttribute = $data['baseUrlAttribute'];
        $this->basePathAttribute = $data['basePathAttribute'];
        $this->uriPathAttribute = $data['uriPathAttribute'];
    }

    /**
     * Determine the base url of the specified request and return a new request object with the information
     * added as attributes.
     *
     * @param ServerRequestInterface $request
     * @return ServerRequestInterface
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public function handleRequest(ServerRequestInterface $request): ServerRequestInterface
    {
        $extractor = new BaseUrlExtractor($request);

        return $request->withAttribute($this->baseUrlAttribute, $extractor->baseUrl())
            ->withAttribute($this->basePathAttribute, $extractor->basePath())
            ->withAttribute($this->uriPathAttribute, $extractor->uriPath());
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($this->handleRequest($request));
    }

    /**
     * @param ServerRequestInterface $request
     * @param callable $next
     * @return ResponseInterface
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public function __invoke(ServerRequestInterface $request, callable $next): ResponseInterface
    {
        return $next($this->handleRequest($request));
    }
}
