<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7BaseUrl;

use BadMethodCallException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Determines the base url of a {@see ServerRequestInterface} and allows retrieval of the determined values.
 *
 * @api usage
 * @since 2.0
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class BaseUrlExtractor
{
    private UriInterface $baseUrl;

    private UriInterface $basePath;

    private UriInterface $uriPath;

    /**
     * Constructor.
     *
     * @param ServerRequestInterface $request
     *        The request to extract the attributes from.
     * @no-named-arguments
     */
    public function __construct(ServerRequestInterface $request)
    {
        $uriPath = $request->getUri()->getPath();
        $basePath = $this->findBasePath($request->getServerParams(), $uriPath);

        if (strlen($basePath) > 0 && mb_strpos($uriPath, $basePath) === 0) {
            $uriPath = mb_substr($uriPath, mb_strlen($basePath));
        }

        if (mb_substr($basePath, -1, 1) === '/') {
            $basePath = mb_substr($basePath, 0, -1);
        }

        if (mb_substr($uriPath, 0, 1) !== '/') {
            $uriPath = '/' . $uriPath;
        }

        $this->baseUrl = $request->getUri()->withPath($basePath)->withQuery('')->withFragment('');
        $this->basePath = $this->baseUrl->withScheme('')->withUserInfo('', null)->withHost('')->withPort(null);
        $this->uriPath = $this->basePath->withPath($uriPath);
    }

    /**
     * Prevent clone.
     *
     * @codeCoverageIgnore
     */
    private function __clone()
    {
    }

    /**
     * Prevent serialize.
     *
     * @return array<string, mixed>
     * @codeCoverageIgnore
     */
    public function __serialize(): array
    {
        throw new BadMethodCallException('Cannot serialize ' . __CLASS__);
    }

    /**
     * Prevent unserialize.
     *
     * @param array<string, mixed> $data
     * @codeCoverageIgnore
     */
    public function __unserialize(array $data): void
    {
        throw new BadMethodCallException('Cannot unserialize ' . __CLASS__);
    }

    /**
     * Return the determined base url.
     *
     * @return UriInterface
     * @api usage
     * @since 2.0
     */
    public function baseUrl(): UriInterface
    {
        return $this->baseUrl;
    }

    /**
     * Return the determined base path.
     *
     * @return UriInterface
     * @api usage
     * @since 2.0
     */
    public function basePath(): UriInterface
    {
        return $this->basePath;
    }

    /**
     * Return the determined uri path.
     *
     * @return UriInterface
     * @api usage
     * @since 2.0
     */
    public function uriPath(): UriInterface
    {
        return $this->uriPath;
    }

    /**
     * Try to detect the base path of the request.
     *
     * This method is copied from `\Blast\BaseUrl\BaseUrlFinder::findBaseUrl()` (version 1.0.0) because the library
     * has not been updated in nearly 2 years and does currently (2020-10-25) not support PHP 8.
     *
     * To allow proper PHP 8 support (and also remove the only functional dependency this library has)
     * the logic has been copied for version 2.0.0, but credit for the actual functionality goes to the
     * authors of the `Blast\BaseUrl` library (https://github.com/mtymek/blast-base-url).
     *
     * @param string[] $serverParams
     * @param string $uriPath
     * @return string
     * @no-named-arguments
     */
    private function findBasePath(array $serverParams, string $uriPath): string
    {
        $filename = array_key_exists('SCRIPT_FILENAME', $serverParams) ?
            $serverParams['SCRIPT_FILENAME'] :
            '';
        $scriptName = array_key_exists('SCRIPT_NAME', $serverParams) ?
            $serverParams['SCRIPT_NAME'] :
            null;
        $phpSelf = array_key_exists('PHP_SELF', $serverParams) ?
            $serverParams['PHP_SELF'] :
            null;
        $origScriptName = array_key_exists('ORIG_SCRIPT_NAME', $serverParams) ?
            $serverParams['ORIG_SCRIPT_NAME'] :
            null;

        if ($scriptName !== null && basename($scriptName) === $filename) {
            $basePath = $scriptName;
        } elseif ($phpSelf !== null && basename($phpSelf) === $filename) {
            $basePath = $phpSelf;
        } elseif ($origScriptName !== null && basename($origScriptName) === $filename) {
            // 1and1 shared hosting compatibility.
            $basePath = $origScriptName;
        } else {
            // Backtrack up the SCRIPT_FILENAME to find the portion
            // matching PHP_SELF.

            $basePath = '/';
            $basename = basename($filename);
            if ($basename !== '') {
                $path = $phpSelf !== null ? trim($phpSelf, '/') : '';
                $basePos = strpos($path, $basename);
                if ($basePos === false) {
                    $basePos = 0;
                }
                $basePath .= substr($path, 0, $basePos) . $basename;
            }
        }

        // If the baseUrl is empty, then simply return it.
        if ($basePath === '') {
            return '';
        }

        // Full base URL matches.
        if (0 === strpos($uriPath, $basePath)) {
            return $basePath;
        }

        // Directory portion of base path matches.
        $baseDir = str_replace('\\', '/', dirname($basePath));
        if (0 === strpos($uriPath, $baseDir)) {
            return $baseDir;
        }

        $basename = basename($basePath);

        // No match whatsoever
        if ($basename === '' || false === strpos($uriPath, $basename)) {
            return '';
        }

        // If using mod_rewrite or ISAPI_Rewrite strip the script filename
        // out of the base path. $pos > 0 makes sure it is not matching a
        // value from PATH_INFO or QUERY_STRING.
        $pos = strpos($uriPath, $basePath);
        if (strlen($uriPath) >= strlen($basePath) && $pos > 0) {
            $basePath = substr($uriPath, 0, $pos + strlen($basePath));
        }

        return $basePath;
    }
}
