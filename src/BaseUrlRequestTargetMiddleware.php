<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7BaseUrl;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Determines the base url of a {@see ServerRequestInterface} and adds the information to the
 * request object as attribute.
 *
 * Also sets the determined uri path as request target.
 *
 * Implements the PSR-15 middleware interface ({@see MiddlewareInterface}) and can also
 * be used as a middleware using the following signature:
 *
 * ResponseInterface $response = $middleware(ServerRequestInterface $request, callable $next)
 *
 * @api usage
 * @since 1.1
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class BaseUrlRequestTargetMiddleware implements MiddlewareInterface
{
    /**
     * Name of the attribute the base url will be stored in.
     */
    private string $baseUrlAttribute;

    /**
     * Constructor.
     *
     * @param string $baseUrlAttribute
     *        Name of the attribute the base url will be stored in.
     * @no-named-arguments
     */
    public function __construct(string $baseUrlAttribute)
    {
        $this->baseUrlAttribute = $baseUrlAttribute;
    }

    /**
     * @return array{baseUrlAttribute: string}
     */
    public function __serialize(): array
    {
        return [
            'baseUrlAttribute' => $this->baseUrlAttribute,
        ];
    }

    /**
     * @param array{baseUrlAttribute: string} $data
     */
    public function __unserialize(array $data): void
    {
        $this->baseUrlAttribute = $data['baseUrlAttribute'];
    }

    /**
     * Determine the base url of the specified request and return a new request object with the information
     * added as attribute.
     *
     * @param ServerRequestInterface $request
     * @return ServerRequestInterface
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public function handleRequest(ServerRequestInterface $request): ServerRequestInterface
    {
        $extractor = new BaseUrlExtractor($request);

        return $request->withRequestTarget((string)$extractor->uriPath())
            ->withAttribute($this->baseUrlAttribute, $extractor->baseUrl());
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @no-named-arguments
     * @api usage
     * @since 2.0
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($this->handleRequest($request));
    }

    /**
     * @param ServerRequestInterface $request
     * @param callable $next
     * @return ResponseInterface
     * @no-named-arguments
     * @api usage
     * @since 1.1
     */
    public function __invoke(ServerRequestInterface $request, callable $next): ResponseInterface
    {
        return $next($this->handleRequest($request));
    }
}
