<?php

declare(strict_types=1);

namespace BjoernGoetschke\Psr7BaseUrl;

use LogicException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Reads the determined base url from a {@see ServerRequestInterface}.
 *
 * Only works with base urls that are added to the request object as {@see UriInterface}, for example the
 * {@see BaseUrlRequestTargetMiddleware}.
 *
 * @api usage
 * @since 1.2
 * @copyright BSD-2-Clause, see LICENSE.txt and README.md files provided with the library source code
 */
final class BaseUrlResolver
{
    /**
     * Name of the attribute the base url is stored in.
     */
    private string $baseUrlAttribute;

    /**
     * Constructor.
     *
     * @param string $baseUrlAttribute
     *        Name of the attribute the base url is stored in.
     * @no-named-arguments
     */
    public function __construct(string $baseUrlAttribute)
    {
        $this->baseUrlAttribute = $baseUrlAttribute;
    }

    /**
     * @return array{baseUrlAttribute: string}
     */
    public function __serialize(): array
    {
        return [
            'baseUrlAttribute' => $this->baseUrlAttribute,
        ];
    }

    /**
     * @param array{baseUrlAttribute: string} $data
     */
    public function __unserialize(array $data): void
    {
        $this->baseUrlAttribute = $data['baseUrlAttribute'];
    }

    /**
     * Return the determined base url object.
     *
     * @param ServerRequestInterface $request
     *        The request object to read the determined base url from.
     * @return UriInterface
     * @no-named-arguments
     * @api usage
     * @since 1.2
     */
    public function baseUrl(ServerRequestInterface $request): UriInterface
    {
        $baseUrl = $request->getAttribute($this->baseUrlAttribute);
        if (!($baseUrl instanceof UriInterface)) {
            throw new LogicException('Base url object not registered in request.');
        }
        return $baseUrl;
    }

    /**
     * Return the determined base uri object.
     *
     * @param ServerRequestInterface $request
     *        The request object to read the determined base url from.
     * @return UriInterface
     * @no-named-arguments
     * @api usage
     * @since 1.2
     */
    public function baseUri(ServerRequestInterface $request): UriInterface
    {
        return $this->baseUrl($request)->withScheme('')->withUserInfo('', null)->withHost('')->withPort(null);
    }
}
