<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl\Unit;

use BjoernGoetschke\Psr7BaseUrl\BaseUrlRequestTargetMiddleware;
use BjoernGoetschke\Psr7BaseUrl\RequestBaseUrlFinderMiddleware;
use BjoernGoetschke\Test\Psr7BaseUrl\TestHelper;
use BjoernGoetschke\Test\Psr7BaseUrl\TestPsr15RequestHandler;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UriInterface;
use Psr\Http\Server\MiddlewareInterface;

final class MiddlewareInvocationTest extends TestCase
{
    private static function dispatchCallable(callable $middleware): ServerRequestInterface
    {
        $calls = 0;
        $passedRequest = null;
        $expectedResponse = new Response(204);
        $actualResponse = $middleware(
            TestHelper::createSimpleServerRequest(),
            function (ServerRequestInterface $request) use (&$calls, &$passedRequest, $expectedResponse) {
                $calls++;
                $passedRequest = $request;
                return $expectedResponse;
            },
        );

        self::assertEquals(
            1,
            $calls,
        );

        self::assertSame(
            $expectedResponse,
            $actualResponse,
        );

        self::assertNotNull(
            $passedRequest,
        );

        return $passedRequest;
    }

    private static function dispatchPsr15(MiddlewareInterface $middleware): ServerRequestInterface
    {
        $calls = 0;
        $passedRequest = null;
        $expectedResponse = new Response(204);
        $actualResponse = $middleware->process(
            TestHelper::createSimpleServerRequest(),
            new TestPsr15RequestHandler(
                function (ServerRequestInterface $request) use (&$calls, &$passedRequest, $expectedResponse) {
                    $calls++;
                    $passedRequest = $request;
                    return $expectedResponse;
                },
            ),
        );

        self::assertEquals(
            1,
            $calls,
        );

        self::assertSame(
            $expectedResponse,
            $actualResponse,
        );

        self::assertNotNull(
            $passedRequest,
        );

        return $passedRequest;
    }

    public function testRequestBaseUrlFinderMiddlewareCallable(): void
    {
        $request = self::dispatchCallable(
            new RequestBaseUrlFinderMiddleware(
                TestHelper::ATTRIBUTE_BASEURL,
                TestHelper::ATTRIBUTE_BASEPATH,
                TestHelper::ATTRIBUTE_URIPATH,
            ),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertSame(
            'http://example.com:12345/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEPATH),
        );

        self::assertSame(
            '/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEPATH),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_URIPATH),
        );

        self::assertSame(
            '/hello/world',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_URIPATH),
        );
    }

    public function testRequestBaseUrlFinderMiddlewarePsr15(): void
    {
        $request = self::dispatchPsr15(
            new RequestBaseUrlFinderMiddleware(
                TestHelper::ATTRIBUTE_BASEURL,
                TestHelper::ATTRIBUTE_BASEPATH,
                TestHelper::ATTRIBUTE_URIPATH,
            ),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertSame(
            'http://example.com:12345/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEPATH),
        );

        self::assertSame(
            '/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEPATH),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_URIPATH),
        );

        self::assertSame(
            '/hello/world',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_URIPATH),
        );
    }

    public function testBaseUrlRequestTargetMiddlewareCallable(): void
    {
        $request = self::dispatchCallable(
            new BaseUrlRequestTargetMiddleware(
                TestHelper::ATTRIBUTE_BASEURL,
            ),
        );

        self::assertSame(
            '/hello/world',
            $request->getRequestTarget(),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertSame(
            'http://example.com:12345/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );
    }

    public function testBaseUrlRequestTargetMiddlewarePsr15(): void
    {
        $request = self::dispatchPsr15(
            new BaseUrlRequestTargetMiddleware(
                TestHelper::ATTRIBUTE_BASEURL,
            ),
        );

        self::assertSame(
            '/hello/world',
            $request->getRequestTarget(),
        );

        self::assertInstanceOf(
            UriInterface::class,
            $request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );

        self::assertSame(
            'http://example.com:12345/dir',
            (string)$request->getAttribute(TestHelper::ATTRIBUTE_BASEURL),
        );
    }
}
