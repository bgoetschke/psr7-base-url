<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl\Unit;

use BjoernGoetschke\Psr7BaseUrl\RequestBaseUrlFinderMiddleware;
use BjoernGoetschke\Test\Psr7BaseUrl\TestHelper;
use PHPUnit\Framework\TestCase;

final class RequestBaseUrlFinderMiddlewareTest extends TestCase
{
    public function testClone(): void
    {
        $originalRequest = TestHelper::createSimpleServerRequest();

        $baseUrlFinder1 = new RequestBaseUrlFinderMiddleware(
            TestHelper::ATTRIBUTE_BASEURL,
            TestHelper::ATTRIBUTE_BASEPATH,
            TestHelper::ATTRIBUTE_URIPATH,
        );
        $baseUrlFinder2 = clone $baseUrlFinder1;

        self::assertNotSame(
            $baseUrlFinder1,
            $baseUrlFinder2,
        );

        $request1 = $baseUrlFinder1->handleRequest($originalRequest);

        $request2 = $baseUrlFinder2->handleRequest($originalRequest);

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEPATH)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEPATH)),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_URIPATH)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_URIPATH)),
        );
    }

    public function testSerialize(): void
    {
        $originalRequest = TestHelper::createSimpleServerRequest();

        $baseUrlFinder1 = new RequestBaseUrlFinderMiddleware(
            TestHelper::ATTRIBUTE_BASEURL,
            TestHelper::ATTRIBUTE_BASEPATH,
            TestHelper::ATTRIBUTE_URIPATH,
        );
        $baseUrlFinder2 = unserialize(serialize($baseUrlFinder1));

        self::assertInstanceOf(
            RequestBaseUrlFinderMiddleware::class,
            $baseUrlFinder2,
        );

        self::assertNotSame(
            $baseUrlFinder1,
            $baseUrlFinder2,
        );

        $request1 = $baseUrlFinder1->handleRequest($originalRequest);

        $request2 = $baseUrlFinder2->handleRequest($originalRequest);

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEPATH)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEPATH)),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_URIPATH)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_URIPATH)),
        );
    }
}
