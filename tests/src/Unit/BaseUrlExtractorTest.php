<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl\Unit;

use BjoernGoetschke\Psr7BaseUrl\BaseUrlExtractor;
use BjoernGoetschke\Test\Psr7BaseUrl\TestHelper;
use PHPUnit\Framework\TestCase;

final class BaseUrlExtractorTest extends TestCase
{
    /**
     * @return mixed[]
     */
    public function dataProvider(): array
    {
        return [
            'access_script_directly' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/script.php',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir/script.php',
                '/dir/script.php',
                '/',
            ],
            'access_with_rewrite_rules' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/hello/world',
            ],
            'access_with_rewrite_rules_and_query_parameters' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world?query=param&additonal=value',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/hello/world',
            ],
            'access_directory' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/',
            ],
            'access_directory_with_trailing_slash' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/',
            ],
            'access_directory_without_script_name' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/',
                    'SCRIPT_FILENAME' => '/root/dir',
                    'SCRIPT_NAME' => '/dir',
                    'PHP_SELF' => '/dir',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/',
            ],
            'access_directory_without_script_name_and_with_trailing_slash' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/',
                    'SCRIPT_FILENAME' => '/root/dir/',
                    'SCRIPT_NAME' => '/dir/',
                    'PHP_SELF' => '/dir/',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/',
            ],
            'access_root_directory' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/',
                    'SCRIPT_FILENAME' => '/root/script.php',
                    'SCRIPT_NAME' => '/script.php',
                    'PHP_SELF' => '/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345',
                '',
                '/',
            ],
            /* test cases copied from Blast\BaseUrl library on 2020-10-26 (version 1.0.0) */
            'blast_baseurl_01' => [
                [
                    'REQUEST_URI' => '/index.php/news/3?var1=val1&var2=val2',
                    'QUERY_URI' => 'var1=val1&var2=val2',
                    'SCRIPT_NAME' => '/index.php',
                    'PHP_SELF' => '/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_02' => [
                [
                    'REQUEST_URI' => '/public/index.php/news/3?var1=val1&var2=val2',
                    'QUERY_URI' => 'var1=val1&var2=val2',
                    'SCRIPT_NAME' => '/public/index.php',
                    'PHP_SELF' => '/public/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/public/index.php',
                ],
                '/public/index.php',
                '/public/index.php',
                '/news/3',
            ],
            'blast_baseurl_03' => [
                [
                    'REQUEST_URI' => '/index.php/news/3?var1=val1&var2=val2',
                    'SCRIPT_NAME' => '/home.php',
                    'PHP_SELF' => '/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_04' => [
                [
                    'REQUEST_URI' => '/index.php/news/3?var1=val1&var2=val2',
                    'SCRIPT_NAME' => '/home.php',
                    'PHP_SELF' => '/home.php',
                    'ORIG_SCRIPT_NAME' => '/index.php',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_05' => [
                [
                    'REQUEST_URI' => '/index.php/news/3?var1=val1&var2=val2',
                    'PHP_SELF' => '/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_06' => [
                [
                    'HTTP_X_REWRITE_URL' => '/index.php/news/3?var1=val1&var2=val2',
                    'PHP_SELF' => '/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_07' => [
                [
                    'ORIG_PATH_INFO' => '/index.php/news/3',
                    'QUERY_STRING' => 'var1=val1&var2=val2',
                    'PHP_SELF' => '/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/index.php',
                '/index.php',
                '/news/3',
            ],
            'blast_baseurl_08' => [
                [
                    'REQUEST_URI' => '/article/archive?foo=index.php',
                    'QUERY_STRING' => 'foo=index.php',
                    'SCRIPT_FILENAME' => '/var/www/zftests/index.php',
                ],
                '',
                '',
                '/article/archive',
            ],
            'blast_baseurl_09' => [
                [
                    'REQUEST_URI' => '/html/index.php/news/3?var1=val1&var2=val2',
                    'PHP_SELF' => '/html/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/html/index.php',
                '/html/index.php',
                '/news/3',
            ],
            'blast_baseurl_10' => [
                [
                    'REQUEST_URI' => '/dir/action',
                    'PHP_SELF' => '/dir/index.php',
                    'SCRIPT_FILENAME' => '/var/web/dir/index.php',
                ],
                '/dir',
                '/dir',
                '/action',
            ],
            'blast_baseurl_11' => [
                [
                    'SCRIPT_NAME' => '/~username/public/index.php',
                    'REQUEST_URI' => '/~username/public/',
                    'PHP_SELF' => '/~username/public/index.php',
                    'SCRIPT_FILENAME' => '/Users/username/Sites/public/index.php',
                    'ORIG_SCRIPT_NAME' => null,
                ],
                '/~username/public',
                '/~username/public',
                '/',
            ],
            'blast_baseurl_12' => [ // ZF2-206
                [
                    'SCRIPT_NAME' => '/zf2tut/index.php',
                    'REQUEST_URI' => '/zf2tut/',
                    'PHP_SELF' => '/zf2tut/index.php',
                    'SCRIPT_FILENAME' => 'c:/ZF2Tutorial/public/index.php',
                    'ORIG_SCRIPT_NAME' => null,
                ],
                '/zf2tut',
                '/zf2tut',
                '/',
            ],
            'blast_baseurl_13' => [
                [
                    'REQUEST_URI' => '/html/index.php/news/3?var1=val1&var2=/index.php',
                    'PHP_SELF' => '/html/index.php/news/3',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/html/index.php',
                '/html/index.php',
                '/news/3',
            ],
            'blast_baseurl_14' => [
                [
                    'REQUEST_URI' => '/html/index.php/news/index.php',
                    'PHP_SELF' => '/html/index.php/news/index.php',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/html/index.php',
                '/html/index.php',
                '/news/index.php',
            ],
            'blast_baseurl_15' => [ // Test when url query contains a full http url
                [
                    'REQUEST_URI' => '/html/index.php?url=http://test.example.com/path/&foo=bar',
                    'PHP_SELF' => '/html/index.php',
                    'SCRIPT_FILENAME' => '/var/web/html/index.php',
                ],
                '/html/index.php',
                '/html/index.php',
                '/',
            ],
            /* test cases to cover edge cases in code copied from Blast\BaseUrl library (version 1.0.0) */
            'blast_baseurl_edge_basename_scriptname_equals_filename' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => 'script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/something.different',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/hello/world',
            ],
            'blast_baseurl_edge_basename_phpself_equals_filename' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => 'script.php',
                    'SCRIPT_NAME' => '/something.different',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/hello/world',
            ],
            'blast_baseurl_edge_basename_origscriptname_equals_filename' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => 'script.php',
                    'SCRIPT_NAME' => '/something.different',
                    'PHP_SELF' => '/something.different',
                    'ORIG_SCRIPT_NAME' => '/dir/script.php',
                ],
                'http://example.com:12345/dir',
                '/dir',
                '/hello/world',
            ],
            'blast_baseurl_edge_empty_resolved_basepath_from_scriptname' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => '',
                    'SCRIPT_NAME' => '',
                    'PHP_SELF' => '/something.different',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345',
                '',
                '/dir/hello/world',
            ],
            'blast_baseurl_edge_empty_resolved_basepath_from_phpself' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => '',
                    'SCRIPT_NAME' => '/something.different',
                    'PHP_SELF' => '',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345',
                '',
                '/dir/hello/world',
            ],
            'blast_baseurl_edge_empty_resolved_basepath_from_origscriptname' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world',
                    'SCRIPT_FILENAME' => '',
                    'SCRIPT_NAME' => '/something.different',
                    'PHP_SELF' => '/something.different',
                    'ORIG_SCRIPT_NAME' => '',
                ],
                'http://example.com:12345',
                '',
                '/dir/hello/world',
            ],
            'blast_baseurl_edge_mismatched_uri' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/something/else',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345',
                '',
                '/something/else',
            ],
            'blast_baseurl_edge_script_name_in_uri_subdirectory' => [
                [
                    '__TEST_REQUEST_URI__' => 'http://example.com:12345/something/else/dir/script.php/hello/world',
                    'SCRIPT_FILENAME' => '/root/dir/script.php',
                    'SCRIPT_NAME' => '/dir/script.php',
                    'PHP_SELF' => '/dir/script.php',
                    'ORIG_SCRIPT_NAME' => '/something.different',
                ],
                'http://example.com:12345/something/else/dir/script.php',
                '/something/else/dir/script.php',
                '/hello/world',
            ],
        ];
    }

    /**
     * @dataProvider dataProvider
     * @param mixed[] $serverParams
     * @param string $expectedBaseUrl
     * @param string $expectedBasePath
     * @param string $expectedUriPath
     */
    public function testBaseUrlExtractor(
        array $serverParams,
        string $expectedBaseUrl,
        string $expectedBasePath,
        string $expectedUriPath
    ): void {
        $extractor = new BaseUrlExtractor(TestHelper::createServerRequest($serverParams));

        self::assertSame(
            $expectedBaseUrl,
            (string)$extractor->baseUrl(),
        );

        self::assertSame(
            $expectedBasePath,
            (string)$extractor->basePath(),
        );

        self::assertSame(
            $expectedUriPath,
            (string)$extractor->uriPath(),
        );
    }
}
