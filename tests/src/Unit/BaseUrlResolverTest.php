<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl\Unit;

use BjoernGoetschke\Psr7BaseUrl\BaseUrlResolver;
use BjoernGoetschke\Test\Psr7BaseUrl\TestHelper;
use GuzzleHttp\Psr7\Uri;
use LogicException;
use PHPUnit\Framework\TestCase;

final class BaseUrlResolverTest extends TestCase
{
    public function testBaseUrlIsReturnedUnmodified(): void
    {
        $resolver = new BaseUrlResolver(TestHelper::ATTRIBUTE_BASEURL);

        $baseUrl = new Uri('http://user:password@www.example.com:42/base?hello=world#test');
        $request = TestHelper::createEmptyServerRequest()->withAttribute(TestHelper::ATTRIBUTE_BASEURL, $baseUrl);

        $resolvedBaseUrl = $resolver->baseUrl($request);

        self::assertSame(
            $baseUrl,
            $resolvedBaseUrl,
        );

        self::assertSame(
            'http',
            $resolvedBaseUrl->getScheme(),
        );

        self::assertSame(
            'user:password@www.example.com:42',
            $resolvedBaseUrl->getAuthority(),
        );

        self::assertSame(
            'user:password',
            $resolvedBaseUrl->getUserInfo(),
        );

        self::assertSame(
            'www.example.com',
            $resolvedBaseUrl->getHost(),
        );

        self::assertSame(
            42,
            $resolvedBaseUrl->getPort(),
        );

        self::assertSame(
            '/base',
            $resolvedBaseUrl->getPath(),
        );

        self::assertSame(
            'hello=world',
            $resolvedBaseUrl->getQuery(),
        );

        self::assertSame(
            'test',
            $resolvedBaseUrl->getFragment(),
        );
    }

    public function testMissingBaseUrlThrowsException(): void
    {
        $resolver = new BaseUrlResolver(TestHelper::ATTRIBUTE_BASEURL);

        $request = TestHelper::createEmptyServerRequest()->withoutAttribute(TestHelper::ATTRIBUTE_BASEURL);

        $this->expectException(LogicException::class);
        $resolver->baseUrl($request);
    }

    public function testBaseUriContainsOnlyExpectedData(): void
    {
        $resolver = new BaseUrlResolver(TestHelper::ATTRIBUTE_BASEURL);

        $baseUrl = new Uri('http://user:password@www.example.com:42/base?hello=world#test');
        $request = TestHelper::createEmptyServerRequest()->withAttribute(TestHelper::ATTRIBUTE_BASEURL, $baseUrl);

        $resolvedBaseUri = $resolver->baseUri($request);

        self::assertNotSame(
            $baseUrl,
            $resolvedBaseUri,
        );

        self::assertSame(
            '',
            $resolvedBaseUri->getScheme(),
        );

        self::assertSame(
            '',
            $resolvedBaseUri->getAuthority(),
        );

        self::assertSame(
            '',
            $resolvedBaseUri->getUserInfo(),
        );

        self::assertSame(
            '',
            $resolvedBaseUri->getHost(),
        );

        self::assertNull(
            $resolvedBaseUri->getPort(),
        );

        self::assertSame(
            '/base',
            $resolvedBaseUri->getPath(),
        );

        self::assertSame(
            'hello=world',
            $resolvedBaseUri->getQuery(),
        );

        self::assertSame(
            'test',
            $resolvedBaseUri->getFragment(),
        );
    }

    public function testClone(): void
    {
        $resolver1 = new BaseUrlResolver(TestHelper::ATTRIBUTE_BASEURL);
        $resolver2 = clone $resolver1;

        self::assertNotSame(
            $resolver1,
            $resolver2,
        );

        $baseUrl = new Uri('http://user:password@www.example.com:42/base?hello=world#test');
        $request = TestHelper::createEmptyServerRequest()->withAttribute(TestHelper::ATTRIBUTE_BASEURL, $baseUrl);

        self::assertSame(
            $baseUrl,
            $resolver2->baseUrl($request),
        );
    }

    public function testSerialize(): void
    {
        $resolver1 = new BaseUrlResolver(TestHelper::ATTRIBUTE_BASEURL);
        $resolver2 = unserialize(serialize($resolver1));

        self::assertInstanceOf(
            BaseUrlResolver::class,
            $resolver2,
        );

        self::assertNotSame(
            $resolver1,
            $resolver2,
        );

        $baseUrl = new Uri('http://user:password@www.example.com:42/base?hello=world#test');
        $request = TestHelper::createEmptyServerRequest()->withAttribute(TestHelper::ATTRIBUTE_BASEURL, $baseUrl);

        self::assertSame(
            $baseUrl,
            $resolver2->baseUrl($request),
        );
    }
}
