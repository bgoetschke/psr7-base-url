<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl\Unit;

use BjoernGoetschke\Psr7BaseUrl\BaseUrlRequestTargetMiddleware;
use BjoernGoetschke\Test\Psr7BaseUrl\TestHelper;
use PHPUnit\Framework\TestCase;

final class BaseUrlRequestTargetMiddlewareTest extends TestCase
{
    public function testClone(): void
    {
        $originalRequest = TestHelper::createSimpleServerRequest();

        $middleware1 = new BaseUrlRequestTargetMiddleware(
            TestHelper::ATTRIBUTE_BASEURL,
        );
        $middleware2 = clone $middleware1;

        self::assertNotSame(
            $middleware1,
            $middleware2,
        );

        $request1 = $middleware1->handleRequest($originalRequest);

        $request2 = $middleware2->handleRequest($originalRequest);

        self::assertSame(
            $request1->getRequestTarget(),
            $request2->getRequestTarget(),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
        );
    }

    public function testSerialize(): void
    {
        $originalRequest = TestHelper::createSimpleServerRequest();

        $middleware1 = new BaseUrlRequestTargetMiddleware(
            TestHelper::ATTRIBUTE_BASEURL,
        );
        $middleware2 = unserialize(serialize($middleware1));

        self::assertInstanceOf(
            BaseUrlRequestTargetMiddleware::class,
            $middleware2,
        );

        self::assertNotSame(
            $middleware1,
            $middleware2,
        );

        $request1 = $middleware1->handleRequest($originalRequest);

        $request2 = $middleware2->handleRequest($originalRequest);

        self::assertSame(
            $request1->getRequestTarget(),
            $request2->getRequestTarget(),
        );

        self::assertSame(
            TestHelper::asString($request1->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
            TestHelper::asString($request2->getAttribute(TestHelper::ATTRIBUTE_BASEURL)),
        );
    }
}
