<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl;

use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;

final class TestHelper
{
    /**
     * @var string
     */
    public const ATTRIBUTE_BASEURL = 'baseUrl';

    /**
     * @var string
     */
    public const ATTRIBUTE_BASEPATH = 'basePath';

    /**
     * @var string
     */
    public const ATTRIBUTE_URIPATH = 'uriPath';

    /**
     * @param mixed[] $serverParams
     * @return ServerRequestInterface
     */
    public static function createServerRequest(array $serverParams): ServerRequestInterface
    {
        $method = $serverParams['REQUEST_METHOD'] ?? 'GET';
        TestCase::assertIsString($method);

        $body = fopen('php://memory', 'r+');
        $headers = [];

        TestCase::assertIsResource($body);

        $uri = 'http://example.com/';
        if (isset($serverParams['__TEST_REQUEST_URI__'])) {
            $uri = $serverParams['__TEST_REQUEST_URI__'];
        } elseif (isset($serverParams['REQUEST_URI'])) {
            $uri = $serverParams['REQUEST_URI'];
        } elseif (isset($serverParams['PHP_SELF'])) {
            $uri = $serverParams['PHP_SELF'];
        }
        TestCase::assertIsString($uri);

        unset($serverParams['__TEST_REQUEST_URI__']);

        return new ServerRequest(
            $method,
            $uri,
            $headers,
            $body,
            '1.1',
            $serverParams,
        );
    }

    public static function createEmptyServerRequest(): ServerRequestInterface
    {
        return self::createServerRequest(
            [
                '__TEST_REQUEST_URI__' => 'http://example.com/',
                'SCRIPT_FILENAME' => '/root/dir/script.php',
                'SCRIPT_NAME' => '/dir/script.php',
                'PHP_SELF' => '/dir/script.php',
                'ORIG_SCRIPT_NAME' => '/something.different',
            ],
        );
    }

    public static function createSimpleServerRequest(): ServerRequestInterface
    {
        return self::createServerRequest(
            [
                '__TEST_REQUEST_URI__' => 'http://example.com:12345/dir/hello/world?query=param&additonal=value',
                'SCRIPT_FILENAME' => '/root/dir/script.php',
                'SCRIPT_NAME' => '/dir/script.php',
                'PHP_SELF' => '/dir/script.php',
                'ORIG_SCRIPT_NAME' => '/something.different',
            ],
        );
    }

    /**
     * @param mixed $value
     * @return string
     */
    public static function asString($value): string
    {
        TestCase::assertTrue(is_scalar($value) || is_object($value) || is_null($value));
        /** @var scalar $value (prevent PHPStan from complaining that objects cannot be cast to strings) */
        return (string)$value;
    }
}
