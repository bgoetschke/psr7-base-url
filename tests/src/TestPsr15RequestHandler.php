<?php

declare(strict_types=1);

namespace BjoernGoetschke\Test\Psr7BaseUrl;

use Closure;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

final class TestPsr15RequestHandler implements RequestHandlerInterface
{
    /**
     * @var Closure(ServerRequestInterface): ResponseInterface
     */
    private Closure $callback;

    /**
     * @param Closure(ServerRequestInterface): ResponseInterface $callback
     */
    public function __construct(Closure $callback)
    {
        $this->callback = $callback;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        return ($this->callback)($request);
    }
}
